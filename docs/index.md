# example docs

This is a general documentation for GLC project. \
This visual representation captures the end-to-end journey, illustrating the seamless
flow from provisioning the foundational elements to intricately configuring the Istio
service mesh across primary remote instances, and finally, orchestrating the application
deployment with precision and efficiency using Argo CD.
![Screenshot](img/terraform___terragrunt_-_pipeline_s_global_architecture__23_.png)
