# Installation


In the subsequent subsection, we will delve into a comprehensive discussion surrounding
all pipeline’s stages. \
• Infra stage
The ”Infrastructure Stage” constitutes a essential phase within the scope of the project,
focusing on the foundational setup and provisioning required to establish a robust en-
vironment for subsequent processes.
In this stage, complex details and configurations related to computing resources, net-
working components, and essential services are carefully orchestrated to create the
infrastructure backbone. This foundation serves as the underlying basis upon which
the entire solution is built.\
To accomplish this with efficiency and consistency, Terragrunt configurations and Ter-
raform modules are employed to automate the deployment of infrastructure compo-
nents. This automation streamlines the process, enhances repeatability, and ensures
adherence to best practices throughout the infrastructure setup, laying the groundwork
for seamless development and operation phases. \
Within this stage, a strategic division unfolds, manifesting in two principal jobs: \
1 - Import Rancher Cluster: \
This pivotal job undertakes the deployment of a Rancher cluster, by leveraging Ter-
ragrunt scripts. These scripts act as orchestrators, intricately invoking the power of
Terraform modules to lay the foundation of the Rancher cluster. \
2 - Deploy K3S Cluster on Equinix: \
The second job within this stage builds upon the foundational work of the first.To
deploy the K3S cluster within Equinix environment, we need to get rancher command
to be applied on K3S cluster. This command encapsulated as a terragrunt output in the
first job is stored as a GitLab artifact. Then before executing the second job, we need
pass the command’s content as terragrunt variable without any human intervention.
In a similar manner, the process involves provisioning the setup of an additional bare
metal server within the Equinix environment. This server is then utilized as the foun-
dation for deploying a K3s cluster. \
 To achieve this, the installation of the K3s cluster is
facilitated by passing the node token provided by the initial K3s server, which operates
as the master node. The newly deployed K3s cluster effectively functions as a K3s
agent or worker node within the overall architecture. This approach streamlines the
expansion of the cluster while maintaining a unified and efficient system for managing
workloads and resources. We mention that Kubeconfigs of EKS and AKS clusters are
stored as protected environment variable in Gitlab CI and been exported throughtout
next stages. \
• Deployment stage \
The deployment stage is a key phase that involves the comprehensive process of de-
ploying Istio across multiple clusters, along with the integration of monitoring and
visualization tools provided as Istio addons. \
Same as the first stage, deployment phase is divided into 2 jobs: \
1 - Istio multi-cluster deployment: \
This job is a script based, responsible for the deployment of Istio’s multi-cluster con-
figuration across three distinct clusters. \
Figure 3.18 highlights how communication is facilitated between services across three
clusters. \
![Screenshot](img/o.png)
In this phase, our focus shifts towards the installation of Istio across three distinct
clusters. This strategic step involves orchestrating the deployment of Istio which can
be divided into three main parts.
Configuring trust between clusters: Generate Root CA:
First, we Created a key pair and certificate for the root Certificate Authority (CA).
Then we generate Intermediate Certificates that were signed by previous root certificate.
These intermediates serve as Istio’s Certificate Authorities (CAs) within each cluster,
signing certificates for workloads.
In each cluster, We created kubernetes secret containing intermediate certificate details.
This intermediate certificate becomes the CA for signing workload certificates specific
to that cluster. In that way, this secret is vital for every cluster’s sidecar proxies to
authenticate certificates from other clusters.
Configuring primary and remote clusters:
first, install Istio’s control plane (istiod) in the primary cluster, and configure an east-
westgateway service there, exposing it with an external IP for inter-cluster communica-
tion. In the remote cluster, use an IstioOperator with the ”remote” profile, customizing
it with the path for sidecar injection and setting the remotePilotAddress to the exter-
nal IP and port of the primary cluster’s eastwestgateway. This configuration allows
Istio in the remote cluster to connect to the primary cluster’s control plane, facilitating
cross-cluster communication and management.
Enableling discovery of services across clusters:
To enable endpoint discovery for a cluster, we generated a remote secret and deploys it
to each primary cluster in the mesh. The remote secret contains credentials, granting
access to the API server in the remote cluster. The control planes will then connect
and discover the service endpoints for the cluster, enabling cross-cluster load balancing
for these services.
2 - Monitoring and visualization tools deployment:
As the second job unfolds, our attention shifts toward the deployment of critical Istio
addons, including Grafana, Kiali, and Jaeger. These addons play an instrumental role
in enriching the Istio ecosystem by introducing advanced monitoring, visualization, and
tracing capabilities shared between kubernetes clusters.
• GitOps stage
The GitOps Stage marks a crucial phase in our operational workflow, during this job,
we deployed Argo CD in the primary cluster The goal here is to establish Argo CD’s
authority over the remote clusters. That’s why we needed to configure RBAC and
required authentications to ensure Argo CD controller can manage resources in the
remote clusters effectively.
Based on helm chart containing YAML configuration for application deployments stored
in git repository, Argo CD automatically deploys application on three clusters through
Argo CD dashboard without having to do anything manually.
